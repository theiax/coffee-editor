import { injectable } from '@theia/core/shared/inversify';
import { LanguageGrammarDefinitionContribution, TextmateRegistry } from '@theia/monaco/lib/browser/textmate';

// @injectable()
// export class BhaiLangContributionPlist implements LanguageGrammarDefinitionContribution {

//     readonly id = 'bhai123';
//     readonly scopeName = 'source.bhaiLang';

//     registerTextmateLanguage(registry: TextmateRegistry) {
//         registry.registerTextmateGrammarScope(this.scopeName, {
//             async getGrammarDefinition() {
//                 const response = await fetch(require('../../data/bhailang-grammar.plist'));
//                 return {
//                     format: 'plist',
//                     content: await response.text(),
//                 }
//             }
//         });
//         registry.mapLanguageIdToTextmateGrammar(this.id, this.scopeName);
//     }
// }

@injectable()
export class BhaiLangContributionJSON implements LanguageGrammarDefinitionContribution {

    readonly id = 'bhailang';
    readonly scopeName = 'source.bhai';

    registerTextmateLanguage(registry: TextmateRegistry) {
        // monaco.languages.register({
        //     id: 'bhailang',
        //     aliases: [
        //         'BhaiLang', 'bhailang'
        //     ],
        //     extensions: [
        //         '.bhai',
        //     ],
        //     mimetypes: [
        //         'text/bhai'
        //     ]
        // });
        // monaco.languages.setLanguageConfiguration('bhailang', this.configuration);

        registry.registerTextmateGrammarScope(this.scopeName, {
            async getGrammarDefinition() {
                return {
                    format: 'json',
                    content: require('../../data/bhailang.tmLanguage.json'),
                }
            }
        });
        registry.mapLanguageIdToTextmateGrammar(this.id, this.scopeName);
    }
    // protected configuration: monaco.languages.LanguageConfiguration = {
    //     'comments': {
    //         'lineComment': '//',
    //         'blockComment': ['/*', '*/']
    //     },
    //     'brackets': [
    //         ['{', '}'],
    //         ['[', ']'],
    //         ['(', ')']
    //     ],
    //     'autoClosingPairs': [
    //         { 'open': '{', 'close': '}' },
    //         { 'open': '[', 'close': ']' },
    //         { 'open': '(', 'close': ')' },
    //         { 'open': "'", 'close': "'", 'notIn': ['string', 'comment'] },
    //         { 'open': '"', 'close': '"', 'notIn': ['string'] },
    //         { 'open': '/**', 'close': ' */', 'notIn': ['string'] }
    //     ],
    //     'surroundingPairs': [
    //         { 'open': '{', 'close': '}' },
    //         { 'open': '[', 'close': ']' },
    //         { 'open': '(', 'close': ')' },
    //         { 'open': "'", 'close': "'" },
    //         { 'open': '"', 'close': '"' },
    //         { 'open': '`', 'close': '`' }
    //     ],
    //     'folding': {
    //         'markers': {
    //             'start': new RegExp('^\\s*//\\s*#?region\\b'),
    //             'end': new RegExp('^\\s*//\\s*#?endregion\\b')
    //         }
    //     }
    // };
}