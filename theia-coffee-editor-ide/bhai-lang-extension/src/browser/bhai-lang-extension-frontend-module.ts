/**
 * Generated using theia-extension-generator
 */
import { BhaiLangContributionJSON } from './bhai-lang-extension-contribution';
import { LanguageGrammarDefinitionContribution } from '@theia/monaco/lib/browser/textmate';

import { ContainerModule } from '@theia/core/shared/inversify';

export default new ContainerModule(bind => {
    // add your contribution bindings here
    bind(LanguageGrammarDefinitionContribution).to(BhaiLangContributionJSON).inSingletonScope();
});
